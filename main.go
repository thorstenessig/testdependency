package testdependency

import (
	"fmt"
)

var version = "0.0.2"

func HelloDependency() string {
	return fmt.Sprintf("Hello Dependency %s", version)
}
